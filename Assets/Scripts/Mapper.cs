﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Mapper : MonoBehaviour
{
    public NavMeshSurface nvp;
    public bool up, down, left, right;
    public int type;

    private bool roomStatus;
    public bool isCleared = false;
    public bool isPlayerInRoom = false;
    private bool isRoomEmpty = false;
    private EnemySpawner enemySpawner = null;
    private List<GameObject> doors = new List<GameObject>();
    private GameObject trapDoor;
    public Material mat;
    private void Start()
    {
        
        enemySpawner = this.transform.Find("Spawner").gameObject.GetComponent<EnemySpawner>();
        foreach (Transform child in transform)
        {
            if(child.gameObject.tag == "Door")
            {
                doors.Add(child.gameObject);
            }
            if (child.gameObject.tag == "TrapDoor")
                trapDoor = child.gameObject;
        }
        nvp.BuildNavMesh();
        ColourChange();
        
    }

    public void ColourChange()
    {
        foreach (var i in doors)
        {
            if (!up)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.up)
                {
                    i.GetComponent<MeshRenderer>().material = mat;
                }
            }
            if (!down)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.down)
                {
                    i.GetComponent<MeshRenderer>().material = mat;
                }
            }
            if (!left)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.left)
                {
                    i.GetComponent<MeshRenderer>().material = mat;
                }
            }
            if (!right)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.right)
                {
                    i.GetComponent<MeshRenderer>().material = mat;
                }
            }
        }
        
    }

    private void SpawnEnemies()
    {
        if (!isCleared)
        {
            enemySpawner.Spawn();
        }
    }
    private void Update()
    {
        if (GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            isRoomEmpty = true;
        }
        else
        {
            
            isRoomEmpty = false;
        }
        if(isRoomEmpty != roomStatus)
        {
            OpenDoors();
            TrapDoor();
            roomStatus = isRoomEmpty;
        }
    }
    public void TrapDoor()
    {
        if (trapDoor != null && isRoomEmpty && isPlayerInRoom)
        {
            trapDoor.GetComponent<MeshRenderer>().enabled = isRoomEmpty;
            trapDoor.GetComponent<Collider>().enabled = isRoomEmpty;
        }
    }

    private void OpenDoors()
    {
        foreach(var i in doors)
        {
            if (up)
            {
                if(i.GetComponent<Doors>().side == Doors.sideDoor.up)
                {
                    OpenRoom(i);
                }
            }
            if (down)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.down)
                {
                    OpenRoom(i);
                }
            }
            if (left)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.left)
                {
                    OpenRoom(i);
                }
            }
            if (right)
            {
                if (i.GetComponent<Doors>().side == Doors.sideDoor.right)
                {
                    OpenRoom(i);
                }
            }
        }
    }
    public void OpenRoom(GameObject door)
    {
        door.GetComponent<MeshRenderer>().enabled = !isRoomEmpty;
        door.GetComponent<Collider>().enabled = !isRoomEmpty;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isPlayerInRoom = true;
            SpawnEnemies();
            RoomManager.instance.UpdateCamera(transform.position.x, transform.position.z);
            
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isPlayerInRoom = false;
            isCleared = true;
        }
    }
}
