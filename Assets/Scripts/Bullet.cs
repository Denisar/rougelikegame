﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float damage;//damage of the bullet
    private float speed;//travel speed of the bullet
    private float distance = 4f;// this is the distance the bullet will travel.
    public float Damage { set { damage = value; } }//Setter to alter the damage of the bullet
    public float Speed { set { speed = value; } }//setter to alter the speed of the bullet
    public float Distance { set { distance = value; } get { return distance; } }
    private Vector3 startPos;

    private Rigidbody rb;//Bullets RigidBody

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        startPos = this.transform.position;
        
    }

    private void FixedUpdate()
    {
        rb.velocity = transform.forward * speed * Time.deltaTime;//Moving the bullet in the direction it is facing after being shot by the player.
    }
    private void Update()
    {
        float diff = Vector3.Distance(startPos, transform.position);
        if (diff >= distance)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<EnemyHealth>())
        {
            collision.gameObject.GetComponent<EnemyHealth>().TakeDamage(damage,this.transform.position);
            Destroy(this.gameObject);
        }
        if(collision.gameObject.tag == "Wall" || collision.gameObject.tag.Contains("Door"))
        {
            Destroy(this.gameObject);
        }
    }
}
