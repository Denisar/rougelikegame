﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    
    #region singleton
    public static RoomManager instance = null;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }
    #endregion

    
    private GameObject mainCamera;
    

    private void Start()
    {

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update()
    {
        if(mainCamera == null)
        {
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        }
    }

    public void UpdateCamera(float x,float z)
    {
        if(mainCamera != null)
            mainCamera.transform.position = new Vector3(x, mainCamera.transform.position.y, z);
    }


}
