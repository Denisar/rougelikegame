﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoving : MonoBehaviour {
	
	private float itemSpeedIncrease = 0;//This will be altered by items to change the movement speed of the player
	private float speed = 300;//Base movement speed of the player
    private float currentSpeed = 0;

    public float ItemSpeedIncrease { get { return itemSpeedIncrease; } set { itemSpeedIncrease += value; UpdateSpeed(); } }//Setter to change the Itemspeed

    private Rigidbody rb;//The players rigidbody

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
        UpdateSpeed();
    }

    private void UpdateSpeed()
    {
        currentSpeed = speed + ItemSpeedIncrease;
    }

    public void MovePlayer(float h, float v)
    {
        Vector3 movement = new Vector3(h, 0, v);
        rb.velocity = movement * currentSpeed * Time.deltaTime;//This will move the player according to the direction inputted
        if (h == 1)//These below handle the turn of the player with arrow keys and firing the bullet on when held/pressed
        {
            if(transform.rotation.y != 180)
                transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else if (h == -1)
        {
            if (transform.rotation.y != 0)
                transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (v == 1)
        {
            if (transform.rotation.y != 90)
                transform.rotation = Quaternion.Euler(0, 90, 0);
        }
        else if (v == -1)
        {
            if (transform.rotation.y != -90)
                transform.rotation = Quaternion.Euler(0, -90, 0);
        }
    }

    

	private void FixedUpdate(){
        MovePlayer(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        /* This will take user input and move player accordingly. 
         * NOTE: This is in fixed update as movement will not need to be percise and the button is held instead of just pressed.*/
        
                            
    }
}
