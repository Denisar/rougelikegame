﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private int currentHealth;
    public int maxHealth = 6;//The health of the player : Subject to change and split 

    private int startingBombs = 3;
    private int bomb;//Bombs are place holders we could do cannon shots or any other consumable 

    private float iFrames = 0.6f;//The invincability frames that the player will revice after taking damage
    private float timer = 0;

    public int Health { get { return currentHealth; } }
    public int Bomb { get { return bomb; } }

    public List<Item> inventory = new List<Item>();

    public void Awake()
    {
        currentHealth = maxHealth;
        startingBombs = bomb;
        GameManager.instance.FindPlayer();
    }

    public void MoveLevel(int ch, List<Item> inv)//This will be accessed by the GameManager to move the player  to a new level.
    {
        foreach(var i in inv)
        {
            AddItem(i);
        }
        currentHealth = ch;
    }
    
    public void AddItem(Item item)// This method will be used when player touches an item. It will add the item to the inventory and distribute the stats accordingly
    {
        this.inventory.Add(item);
        this.maxHealth += item.health;
        if (item.health > 0)
            this.currentHealth += item.health;
        if (maxHealth < currentHealth)
            currentHealth = maxHealth;
        this.bomb += item.bombs;
        this.GetComponent<PlayerMoving>().ItemSpeedIncrease += item.moveSpeed;
        this.GetComponent<PlayerAim>().ItemDamage += item.damage;
        this.GetComponent<PlayerAim>().ItemAttackSpeed += item.attackSpeed;
        this.GetComponent<PlayerAim>().ItemBulletSpeed += item.bulletSpeed;
        this.GetComponent<PlayerAim>().ItemDistance += item.bulletDistance;
    }


    public void TakeDamage(GameObject gm)//This will deal damage to the player as long as they can take damage;
    {
        if(timer <= 0)
        {
            currentHealth -= 1;
            timer = iFrames;
        }
        if(currentHealth <= 0)
        {
            PlayerDie();
        }
    }

    private void PlayerDie()
    {
        Destroy(this.gameObject);
    }

    public void FixedUpdate()
    {
        if (timer > 0)//This will count down the iFrame the player has
        {
            timer-=Time.deltaTime;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "EnemyBullet")//If the player touches a enemy bullet or enemy it will deal damage;
        {
            TakeDamage(collision.gameObject);
        }

        if(collision.gameObject.tag == "Item")
        {
            AddItem(collision.gameObject.GetComponent<Item>());
            Destroy(collision.gameObject, 0);
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "EnemyBullet")//If the player touches a enemy bullet or enemy it will deal damage;
        {
            TakeDamage(collision.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "TrapDoor")
        {
            GameManager.instance.NextLevel(currentHealth,inventory);
        }
    }


}
