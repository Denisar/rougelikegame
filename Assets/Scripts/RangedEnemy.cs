﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RangedEnemy : MonoBehaviour
{
    private GameObject player;//This will be the player

    private float speed = 2;//This is the speed that the enemy will move at

    private NavMeshAgent agent;// This is used as the AI for the enemy

    private float range = 15;
    [SerializeField]
    private GameObject bullet;
    float attackSpeed = 0.8f;
    float timer;
    private System.Random rand = new System.Random();
    private Rigidbody rb;
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (player != null)
        {
            agent.SetDestination(player.transform.position);// This will move the enemy to the players location
            transform.LookAt(player.transform);
            if (timer <= 0)
            {
                Instantiate(bullet, transform.position, transform.rotation);
                timer = attackSpeed;
            }
        }
        
        if (timer > 0)
            timer -= Time.deltaTime;

    }

    
}
