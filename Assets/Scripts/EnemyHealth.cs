﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    private float maxHealth;
    private float currentHealth;

    private void Awake()
    {
        if(maxHealth <= 0)
        {
            Debug.Log("Enemy Health is missing");
        }
        currentHealth = maxHealth;
    }

    public void TakeDamage(float dmg, Vector3 pos)
    {
        transform.position += (transform.position - pos);
        currentHealth -= dmg;
        if(currentHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    
}
