﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    public GameObject[] enemy;

    public void Spawn()
    {
        if (enemy.Length != 0)
        {
            if (enemy[0] != null)
            {
                System.Random rand = new System.Random();
                int chosen = rand.Next(0, enemy.Length);
                if (enemy[chosen].gameObject.tag == "Enemy")
                    Instantiate(enemy[chosen], this.transform.position, this.transform.rotation);
            }
        }
    }
}
