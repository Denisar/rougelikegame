﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    public GameObject player;
    void Start()
    {
        if(GameObject.FindGameObjectWithTag("Player") == null)
        {
            Instantiate(player, new Vector3(0, -9.5f, 0), player.transform.rotation);
        }
    }

   
}
