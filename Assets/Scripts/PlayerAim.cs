﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour {

    [SerializeField]
    private GameObject defaultBullet;//This is the default bullet incase there is no bullet set

    private GameObject bullet = null;//This is the bullet that will be used by the player

    #region Player stats
    private float damage = 25;//This is the base damage for the character
    private float attackSpeed = 0.8f;//This is the base attack speed for the character

    private float bulletSpeed = 250f; //This affects the speed at which the bullet traves.

    private float itemDamage = 0; //This is going to alter the damage based on items
    private float itemAttackSpeed = 0; //This is going to alter the attack speed based on items
    private float itemBulletSpeed = 0;
    private float itemDistance = 0;
    private float shootTimer = 0; //This is used to track attack speed

    private float currentDamage;//This will be used for the damage
    private float currentAttackSpeed;//This will be used for the attack speed
    private float currentBulletSpeed;//This is the bullet speed.


    public float ItemDamage { get { return itemDamage; } set { itemDamage += value; UpdateStats(); } } //This is used to alter the damage of the bullets
    public float ItemAttackSpeed { get { return itemAttackSpeed; } set { itemAttackSpeed += value; UpdateStats(); } } //This can be used to increase or decrease attack speed
    public float ItemBulletSpeed { get { return itemBulletSpeed; } set { itemBulletSpeed += value; UpdateStats(); } }
    public float ItemDistance { get { return itemDistance; } set { itemDistance += value; } }
    public GameObject Bullet { set { bullet = value; } } //This a setter for the bullet 
    #endregion

    private void Awake()
    {
        if (bullet == null)//If there is no other bullet this will use the default bullet for the character
            bullet = defaultBullet;

        UpdateStats();
    }

    private void UpdateStats()
    {
        currentAttackSpeed = attackSpeed + itemAttackSpeed;
        if (currentAttackSpeed <= 0.1f)
            currentAttackSpeed = 0.1f;

        currentDamage = damage + itemDamage;
        if(currentDamage <= 1)
        {
            currentDamage = 1;
        }
        currentBulletSpeed = bulletSpeed + itemBulletSpeed;
        if(currentBulletSpeed <= 50)
        {
            currentBulletSpeed = 50;
        }
    }

    public void Update()
    {
        if (Input.GetAxisRaw("FireHorizontal") == 1)//These below handle the turn of the player with arrow keys and firing the bullet on when held/pressed
        {
            transform.rotation = Quaternion.Euler(0,180,0);
            Shoot(0.2f,0);
        }
        else if(Input.GetAxisRaw("FireHorizontal") == -1)
        {
            transform.rotation = Quaternion.Euler(0,0, 0);
            Shoot(-0.2f, 0);
        }
        else if(Input.GetAxisRaw("FireVertical") == 1)
        {
            transform.rotation = Quaternion.Euler(0, 90, 0);
            Shoot(0f, 0.2f);
        }
        else if (Input.GetAxisRaw("FireVertical") == -1)
        {
            transform.rotation = Quaternion.Euler(0,-90, 0);
            Shoot(0f, -0.2f);
        }
    }
    public void FixedUpdate()
    {
        if (shootTimer > 0)//this is the timer for shooting
            shootTimer -= Time.deltaTime;
    }

    public void Shoot(float h, float v)
    {
        if (shootTimer <= 0)//If the timer is below or equal to 0 the player is able to shoot
        {
            shootTimer = currentAttackSpeed;
            GameObject newBullet = Instantiate(bullet, transform.position + new Vector3(h,0,v), Quaternion.Euler(0,transform.rotation.eulerAngles.y-90,0));
            var b = newBullet.GetComponent<Bullet>();
            Physics.IgnoreCollision(newBullet.GetComponent<Collider>(), GetComponent<Collider>());
            b.Speed = currentBulletSpeed;
            b.Damage = currentDamage;
            if(itemDistance <= 4)
            {
                itemDistance = 3;
            }
            b.Distance += itemDistance;
        }
    }

}
