﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMelee : MonoBehaviour
{
    private GameObject player;//This will be the player

    private float speed = 2;//This is the speed that the enemy will move at

    private NavMeshAgent agent;// This is used as the AI for the enemy

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
    }

    private void FixedUpdate()
    {
        if (player != null)
        {
            agent.SetDestination(player.transform.position);// This will move the enemy to the players location
            transform.LookAt(player.transform);
        }
    }
}
