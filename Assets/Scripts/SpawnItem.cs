﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItem : MonoBehaviour
{
    [SerializeField]
    private GameObject[] items;

    GameObject itemLocation;

    private void Awake()
    {
        foreach(Transform i in transform)
        {
            if (i.gameObject.tag == "ItemLocation")
                itemLocation = i.gameObject;
        }
        System.Random rand = new System.Random();
        int num = rand.Next(0, items.Length - 1);
        Instantiate(items[num], itemLocation.transform.position,itemLocation.transform.rotation);
    }
}
