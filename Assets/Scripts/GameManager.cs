﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public static GameObject Player = null;
    private int currentHealth = 6;
    private List<Item> storedItems = new List<Item>();
    private int levelNumber = 1;
    public int LevelNumber { get { return levelNumber; } }
    #region Singleton
    private void Awake()//This is a singleton so that any script can access this one.
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    #endregion


    public int GetCH { get { return currentHealth; } }
    
    public List<Item> GetInv { get { return storedItems; } }

    public void NextLevel(int currentHealth, List<Item> inv)
    {
        levelNumber += 1;
        this.currentHealth = currentHealth;
        this.storedItems = inv;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    

    public void FindPlayer()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Player.GetComponent<Player>().MoveLevel(currentHealth, storedItems);

    }

}
