﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{ 
    public sideDoor side;

    public enum sideDoor
    {
        up,
        right,
        left,
        down
    }


    private void Awake()
    {
        if (this.gameObject.name.Contains("Top"))
        {
            side = sideDoor.up;
        }else if (this.gameObject.name.Contains("Right"))
        {
            side = sideDoor.right;
        }
        else if (this.gameObject.name.Contains("Bottom"))
        {
            side = sideDoor.down;
        }
        else if (this.gameObject.name.Contains("Left"))
        {
            side = sideDoor.left;
        }
    }
    
}
