﻿using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public Image ItemImage;
    private string itemName;//This is the name of the item
    public int health;//This is the amount of health the item will give/take
    public float damage;//This is the amount of damage the item will give/take
    public float attackSpeed;//This is the amount of attack speed the item will give/take
    public float moveSpeed;//This is the amount of move speed the item will give/take
    public float bulletSpeed;
    public float bulletDistance;
    public int bombs;//this will give the player bombs

    private void Awake()
    {
        itemName = this.gameObject.name;
    }
}
