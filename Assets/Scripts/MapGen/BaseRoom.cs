﻿
using UnityEngine;

public class BaseRoom 
{
    public Vector3 gridPos;

    public int type;

    public bool doorTop, doorBot, doorLeft, doorRight;

    public BaseRoom(Vector3 _gridPos,int _type)
    {
        this.gridPos = _gridPos;
        this.type = _type;
    }
}
