﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGen : MonoBehaviour
{
    Vector2 worldSize = new Vector2(8 , 8);//Max size of the map 256 rooms at max. each room takes half spot. Default should be 8*8

    BaseRoom[,] rooms;

    List<Vector2> takenPositions = new List<Vector2>();

    int gridSizeX;
    int gridSizeY;
    int baseNumberOfRooms = 4;// Base should be 4
    int numberOfRooms = 0;//Base Starting Number of Normal rooms

    public GameObject roomWhiteObj;
    public GameObject startRoom;
    public GameObject itemRoom;
    public GameObject bossRoom;

    private void Awake()
    {
        RoomMaker(GameManager.instance.LevelNumber);
    }

    public void RoomMaker(int lsm)
    {
        numberOfRooms = baseNumberOfRooms * lsm;
        if (numberOfRooms >= (worldSize.x * 2) * (worldSize.y * 2))
        {
            numberOfRooms = Mathf.RoundToInt((worldSize.x * 2) * (worldSize.y * 2));
        }
        gridSizeX = Mathf.RoundToInt(worldSize.x);
        gridSizeY = Mathf.RoundToInt(worldSize.y);
        CreateRooms();
        SetRoomDoors();
        DrawMap();
    }

    void CreateRooms()
    {
        rooms = new BaseRoom[gridSizeX * 2, gridSizeY * 2];
        rooms[gridSizeX, gridSizeY] = new BaseRoom(Vector2.zero, 1);
        takenPositions.Insert(0, Vector2.zero);
        Vector2 checkPos = Vector2.zero;

        float randomCompare = 0.2f, randomCompareStart = 0.2f, randomCompareEnd = 0.01f;

        for (int i = 0; i < numberOfRooms - 1; i++)
        {
            float randomPerc = ((float)i) / (((float)numberOfRooms - 1));
            randomCompare = Mathf.Lerp(randomCompareStart, randomCompareEnd, randomPerc);
            //get new pos
            checkPos = NewPosition();
            if (NumberOfNeighbors(checkPos, takenPositions) > 1 && Random.value > randomCompare )
            {
                int iterations = 0;
                do
                {
                    checkPos = SelectiveNewPosition();
                    iterations++;
                } while (NumberOfNeighbors(checkPos, takenPositions) > 1 && iterations < 100);
                if (iterations >= 50)
                    print("Error in generation"); 
            }

            rooms[(int)checkPos.x + gridSizeX, (int)checkPos.y + gridSizeY] = new BaseRoom(new Vector3(checkPos.x,0,checkPos.y), 0);
            takenPositions.Insert(0, checkPos);
        }
        do { 
            checkPos = SelectiveNewPosition();
        } while (NumberOfNeighbors(checkPos, takenPositions) != 1);
        rooms[(int)checkPos.x + gridSizeX, (int)checkPos.y + gridSizeY] = new BaseRoom(new Vector3(checkPos.x, 0, checkPos.y), 2);
        takenPositions.Insert(0, checkPos);

        do
        {
            checkPos = SelectiveNewPosition();
        } while (NumberOfNeighbors(checkPos, takenPositions) != 1 || CheckForSpecialRooms(checkPos,takenPositions,2) == false);
        rooms[(int)checkPos.x + gridSizeX, (int)checkPos.y + gridSizeY] = new BaseRoom(new Vector3(checkPos.x, 0, checkPos.y), 3);
        takenPositions.Insert(0, checkPos);

    }

    Vector2 NewPosition()
    {
        int x = 0, y = 0;
        Vector2 checkingPos = Vector2.zero;
        do
        {
            int index = Mathf.RoundToInt(Random.value * (takenPositions.Count - 1));
            x = (int)takenPositions[index].x;
            y = (int)takenPositions[index].y;
            bool UpDown = (Random.value < 0.5f);
            bool positive = (Random.value < 0.5f);
            if (UpDown)
            {
                if (positive)
                {
                    y += 1;
                }
                else
                {
                    y -= 1;
                }
            }
            else
            {
                if (positive)
                {
                    x += 1;
                }
                else
                {
                    x -= 1;
                }
            }
            checkingPos = new Vector2(x, y);
        } while (takenPositions.Contains(checkingPos) || x >= gridSizeX || x < -gridSizeX || y >= gridSizeY || y < -gridSizeY);
        return checkingPos;
    }
    Vector2 SelectiveNewPosition()
    {
        int index = 0, inc = 0;
        int x = 0, y = 0;
        Vector2 checkingPos = Vector2.zero;
        do
        {
            inc = 0;
            do
            {
                index = Mathf.RoundToInt(Random.value * (takenPositions.Count - 1));
                inc++;
            } while (NumberOfNeighbors(takenPositions[index], takenPositions) > 1 && inc < 100);
            x = (int)takenPositions[index].x;
            y = (int)takenPositions[index].y;
            bool UpDown = (Random.value < 0.5f);
            bool positive = (Random.value < 0.5f);
            if (UpDown)
            {
                if (positive)
                {
                    y += 1;
                }
                else
                {
                    y -= 1;
                }
            }
            else
            {
                if (positive)
                {
                    x += 1;
                }
                else
                {
                    x -= 1;
                }
            }
            checkingPos = new Vector2(x, y);
        } while (takenPositions.Contains(checkingPos) || x >= gridSizeX || x < -gridSizeX || y >= gridSizeY || y < -gridSizeY);
        if (inc >= 100)
        {
            print("Error map gen");
        }
        return checkingPos;
    }

    int NumberOfNeighbors(Vector2 checkingPos, List<Vector2> usedPos)
    {

        int ret = 0;
        if (usedPos.Contains(checkingPos + Vector2.right))
        {
            ret++;
        }
        if(usedPos.Contains(checkingPos + Vector2.left))
        {
            ret++;
        }
        if (usedPos.Contains(checkingPos + Vector2.up))
        {
            ret++;
        }
        if (usedPos.Contains(checkingPos + Vector2.down))
        {
            ret++;
        }

        return ret;
    }

    bool CheckForSpecialRooms(Vector2 checkingPos, List<Vector2> usedPos, int type)
    {
        
        if (usedPos.Contains(checkingPos + Vector2.right))
        {
            Vector2 test = checkingPos + Vector2.right;
            if(rooms[(int)test.x+gridSizeX,(int)test.y+gridSizeY].type == type)
            {
                return false; 
            }
        }
        if (usedPos.Contains(checkingPos + Vector2.left))
        {
            Vector2 test = checkingPos + Vector2.left;
            if (rooms[(int)test.x + gridSizeX, (int)test.y + gridSizeY].type == type)
            {
                return false;
            }
        }
        if (usedPos.Contains(checkingPos + Vector2.up))
        {
            Vector2 test = checkingPos + Vector2.up;
            if (rooms[(int)test.x + gridSizeX, (int)test.y + gridSizeY].type == type)
            {
                return false; 
            }
        }
        if (usedPos.Contains(checkingPos + Vector2.down))
        {
            Vector2 test = checkingPos + Vector2.down;
            if (rooms[(int)test.x + gridSizeX, (int)test.y + gridSizeY].type == type)
            {
                return false;
            }
        }

        return true;
    }

    void SetRoomDoors()
    {
        for(int x = 0; x < (gridSizeX * 2); x++)
        {
            for(int y = 0; y < (gridSizeY * 2); y++)
            {
                if(rooms[x,y] == null)
                {
                    continue;
                }
                if (y - 1 < 0)
                {
                    rooms[x, y].doorBot = false;
                }
                else
                {
                    rooms[x, y].doorBot = (rooms[x, y - 1] != null);
                }
                if (y + 1 >= gridSizeY * 2)
                {
                    rooms[x, y].doorTop = false;
                }
                else
                {
                    rooms[x, y].doorTop = (rooms[x, y + 1] != null);
                }
                if (x - 1 < 0)
                {
                    rooms[x, y].doorLeft = false;
                }
                else
                {
                    rooms[x, y].doorLeft = (rooms[x-1, y] != null);
                }
                if (x + 1 >= gridSizeX * 2)
                {
                    rooms[x, y].doorRight = false;
                }
                else
                {
                    rooms[x, y].doorRight = (rooms[x+1, y ] != null);
                }
            }
        }
    }
    void DrawMap()
    {
        foreach(BaseRoom room in rooms)
        {
            if(room == null)
            {
                continue;
            }
            Vector3 drawPos = room.gridPos;
            drawPos.x *= 42;
            drawPos.y = -10;
            drawPos.z *= 23;
            Mapper mapper;
            if(room.type == 1)
            {
                mapper = Object.Instantiate(startRoom, drawPos, Quaternion.identity).GetComponent<Mapper>();
            }else if(room.type == 2)
            {
                
                mapper = Object.Instantiate(itemRoom, drawPos, Quaternion.identity).GetComponent<Mapper>();
            }
            else if (room.type == 3)
            {
                mapper = Object.Instantiate(bossRoom, drawPos, Quaternion.identity).GetComponent<Mapper>();
            }
            else
            {
                mapper = Object.Instantiate(roomWhiteObj, drawPos, Quaternion.identity).GetComponent<Mapper>();
            }
            mapper.type = room.type;
            mapper.up = room.doorTop;
            mapper.down = room.doorBot;
            mapper.right = room.doorRight;
            mapper.left = room.doorLeft;
        }
    }
}
